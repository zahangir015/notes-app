const { default: chalk } = require("chalk");
const { Console } = require("console");
const fs = require("fs");
const { title } = require("process");

const addNote = (title, body) => {
  const notes = loadnotes();
  //const duplicateNotes = notes.filter((note) => note.title === title)
  const duplicateNote = notes.find((note) => note.title === title) // returns undefined if not found
  if (!duplicateNote) {
    notes.push({
      title: title,
      body: body,
    });

    saveNotes(notes);
    console.log(chalk.black.bgGreen("New note added!"));
  } else {
    console.log(chalk.black.bgYellow("New title already taken!"));
  }
}

const saveNotes = (notes) => {
  const dataJSON = JSON.stringify(notes);
  fs.writeFileSync("notes.json", dataJSON);
}

const removeNote = (title) => {
  const notes = loadnotes();

  // const keptNotes = notes.filter((note) => {
  //   return note.title !== title;
  // });

  const keptNotes = notes.filter((note) => note.title !== title)

  if (notes.length > keptNotes.length) {
    console.log(chalk.green.inverse("Note removed!"));
    saveNotes(keptNotes);
  } else {
    console.log(chalk.yellow.inverse("No note found!"));
  }
}

const listNotes = () => {
  const notes = loadnotes()
  console.log(chalk.inverse('Your notes'))
  notes.forEach(note => {
      console.log(note.title)
  });
}

const readNote = (title) => {
  const notes = loadnotes();
  const foundNote = notes.find((note) => note.title === title)

  if(foundNote) {
    console.log(chalk.green.inverse(foundNote.title))
    console.log(foundNote.body)
  } else {
    console.log(chalk.red.inverse('Not found'))
  }
  
}

const loadnotes = () => {
  try {
    const databuffer = fs.readFileSync("notes.json");
    const dataJSON = databuffer.toString();
    return JSON.parse(dataJSON);
  } catch (e) {
    return [];
  }
}

module.exports = {
  addNote: addNote,
  removeNote: removeNote,
  listNotes: listNotes,
  readNote: readNote,
};
